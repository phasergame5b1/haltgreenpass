import Phaser from "phaser";
import Boot from "./Scenes/Boot";

window.addEventListener("load", () => {
  // Fa partire il gioco e la scena principale
  let config: any = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    pixelArt: true,
    //disableContextMenu: true,
    scene: Boot,
    scale: {
      parent: 'contenitore-gioco',
      autoCenter: Phaser.Scale.Center.CENTER_BOTH,
      //mode: Phaser.Scale.FIT,
      width: 1440,
      height: 1024,
    },
    physics: {
      default: "arcade",
      arcade: {
        debug: false,
    }
    },
    scenes: ["Boot", "Preloader", "MainMenu", "Main", "UI-scene"]
  };

  var game: Phaser.Game = new Phaser.Game(config);
  let preloaderScene: Phaser.Scene = new Boot();
});
