import { GameObjects, Physics } from "phaser";
import { DebuggerStatement } from "typescript";
import CI from "./CI";
import NPC from "./GameObjects/NPC";
import Player from "./GameObjects/Player";
import { CheckNPC } from "./Scenes/CheckNPC";
import Main from "./Scenes/Main";

export default class NPCsManager {
    npcs: Physics.Arcade.Group;
    scene: Main;
    player: Player;
    playerDepth: number = 399;
    checkedNpc: NPC = null;
    isPlayerCollidingWithNpc = false;
    openCISound: Phaser.Sound.BaseSound;
    keys: any;

    static i = 0;

    constructor(scene: Main, structures) {
        this.scene = scene; 
        this.npcs = scene.physics.add.group();
        this.npcs.runChildUpdate = true;
        this.addPlayer();
        this.generateNPCs();

        this.openCISound = this.scene.sound.add("open_ci_sfx");

        this.keys = this.scene.input.keyboard.addKeys({
            e: 'e',
            space: 'space'
        });

        // giocatore + npc
        scene.physics.add.overlap(this.player.fullBodyCollider, this.npcs, null, (obj1, obj2) => { this.collidePlayerWithNpc(obj1, obj2) }, this);
        // giocatore + strutture
        scene.physics.add.overlap(this.player.fullBodyCollider, structures, null, (obj1, obj2) => { this.depthSortStructure(obj1, obj2); }, this);
        // npc + strutture
        scene.physics.add.overlap(this.npcs, structures, null, (obj1, obj2) => { this.setAnimsOnCollideWithStructure(obj1, obj2); }, this);
    }

    update(delta): void {
        this.player.update(delta);
        this.depthSortStructure();
    }

    depthSortStructure() {
        this.scene.structGroup.getChildren().forEach((body) => {
            body.setDepth(body.y);
        })
    }

    setAnimsOnCollideWithStructure (npc, structure) {
        if (structure.y > npc.y && this.checkVelocityAndStaticity(npc)) {
            npc.previousVelocity.y = 1;
        } else if (structure.y < npc.y && this.checkVelocityAndStaticity(npc)) {
            npc.previousVelocity.y = -1;
        }   
    }

    checkVelocityAndStaticity(npc) {
        return npc.velocity.x == 0 && npc.velocity.y == 0 && npc.static == false;
    }

    collidePlayerWithNpc(player, npc: NPC) {
        if (this.checkedNpc === null && !npc.static) {
            if (!npc.checked) {
                npc.headIconLogo.timer = 0;

                if (this.keys.e.isDown || this.keys.space.isDown) {
                    this.openCISound.play();
                    this.openCheckMenu(npc);
                }
            }
            else if (!npc.hasToDestroy) {
                npc.headIconChecked.timer = 0;
            }
            else if (npc.checked && npc.hasToDestroy){
                npc.headIconWrong.timer = 0;
            }
        }
    }

    openCheckMenu(npc) {
        let checkMenu = new CheckNPC({ scene: this.scene, NPC: npc, NPCsManager: this })
        this.scene.scene.add("CheckNPC", checkMenu, true);
        this.checkedNpc = npc;
        npc.velocity = new Phaser.Math.Vector2(0 ,0);
        npc.beingChecked = true;
    }


    addPlayer() {
        this.player = new Player({x: 100, y: 200, scene: this.scene, character: "player"});
        this.scene.cameras.main.startFollow(this.player).setZoom(4)
        this.scene.cameras.main.setBounds(0, 0, 60.4*16, 37*16);
        this.player.setDepth(this.playerDepth);
    }

    generateNPCs(): void {
        let npcPoints = this.scene.pathfinder.npcPoints;
        let identityCard: CI = new CI();

        for (let i = 0; i < this.scene.npcsToSpawn; i++) {
            let randomPoint = npcPoints[CI.randomNumberTo(npcPoints.length)];
            identityCard = new CI();
            this.npcs.add(new NPC({
                scene: this.scene, 
                x: randomPoint.x * 16, 
                y: randomPoint.y * 16,
                CI: identityCard,
                character: identityCard.character,
                exploresMap: true,
                pathfinder: this.scene.pathfinder
            }));
        }

        // NPC receptionist
        identityCard = new CI();
        this.npcs.add(new NPC({
            scene: this.scene, 
            x: 225, 
            y: 220,
            CI: identityCard,
            character: identityCard.character,
            exploresMap: false,
            pathfinder: this.scene.pathfinder,
            static: true
        }));
        identityCard = new CI();
        this.npcs.add(new NPC({
            scene: this.scene, 
            x: 275, 
            y: 220,
            CI: identityCard,
            character: identityCard.character,
            exploresMap: false,
            pathfinder: this.scene.pathfinder,
            static: true
        }));
        identityCard = new CI();
        this.npcs.add(new NPC({
            scene: this.scene, 
            x: 275+50, 
            y: 220,
            CI: identityCard,
            character: identityCard.character,
            exploresMap: false,
            pathfinder: this.scene.pathfinder,
            static: true
        }));

        // NPC cassieri
        identityCard = new CI();
        this.npcs.add(new NPC({
            scene: this.scene, 
            x: 225, 
            y: 350,
            CI: identityCard,
            character: identityCard.character,
            exploresMap: false,
            pathfinder: this.scene.pathfinder,
            static: true
        }).play(identityCard.character + "_idle_down"));

        identityCard = new CI();
        this.npcs.add(new NPC({
            scene: this.scene, 
            x: 225, 
            y: 415,
            CI: identityCard,
            character: identityCard.character,
            exploresMap: false,
            pathfinder: this.scene.pathfinder,
            static: true
        }).play(identityCard.character + "_idle_down"));

        identityCard = new CI();
        this.npcs.add(new NPC({
            scene: this.scene, 
            x: 225, 
            y: 420 + 60,
            CI: identityCard,
            character: identityCard.character,
            exploresMap: false,
            pathfinder: this.scene.pathfinder,
            static: true
        }).play(identityCard.character + "_idle_down"));

        identityCard = new CI();
        this.npcs.add(new NPC({
            scene: this.scene, 
            x: 225, 
            y: 420 + 60 + 60,
            CI: identityCard,
            character: identityCard.character,
            exploresMap: false,
            pathfinder: this.scene.pathfinder,
            static: true
        }).play(identityCard.character + "_idle_down"));

        //butcher NPCs
        identityCard = new CI();
        this.npcs.add(new NPC({
            scene: this.scene, 
            x: 548, 
            y: 400,
            CI: identityCard,
            character: identityCard.character,
            exploresMap: false,
            pathfinder: this.scene.pathfinder,
            static: true
        }).play(identityCard.character + "_idle_down"));

        identityCard = new CI();
        this.npcs.add(new NPC({
            scene: this.scene, 
            x: 754, 
            y: 408,
            CI: identityCard,
            character: identityCard.character,
            exploresMap: false,
            pathfinder: this.scene.pathfinder,
            static: true
        }).play(identityCard.character + "_idle_down"));

        identityCard = new CI();
        this.npcs.add(new NPC({
            scene: this.scene, 
            x: 880, 
            y: 403,
            CI: identityCard,
            character: identityCard.character,
            exploresMap: false,
            pathfinder: this.scene.pathfinder,
            static: true
        }).play(identityCard.character + "_idle_down"));
    }


}