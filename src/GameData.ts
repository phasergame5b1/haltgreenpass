export var Globals = {
    matchSeconds: 180,
    maleCharacters: 9,
    femaleCharacters: 7,
    smallFont: {
        fontFamily: "dogica",
        fontSize: "14px",
        color: '#fff',
        stroke: '#000',
        strokeThickness: 5,
    },
    normalFont: {
        fontFamily: "dogica",
        fontSize: "28px",
        color: '#fff',
        stroke: '#000',
        strokeThickness: 5,
    },
}
