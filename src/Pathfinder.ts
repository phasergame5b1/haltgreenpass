import EasyStar from "easystarjs";
import { Tilemaps } from "phaser";
import lodash from "lodash";

export default class Pathfinder {
    scene: Phaser.Scene;
    finder: EasyStar.js;
    map: Tilemaps.Tilemap;
    collisionsTileset: Tilemaps.Tileset;
    grid = [];
    npcPointsTiles = [];
    npcPoints = [];
    occupiedPoints: { [key: number]: boolean } = {};

    constructor(data) {
        this.map = data.map;
        this.collisionsTileset = data.collisionsTileset;
        this.scene = data.scene;
        this.finder = new EasyStar.js();
        this.finder.disableDiagonals();
        this.finder.disableCornerCutting();
        this.populateMapArray();
        this.setAcceptableTiles();
        this.setNpcPoints();
    }

    populateMapArray() {
        for (let y = 0; y < this.map.height; y++) {
            let gridCol = [];
            let npcPointsCol = [];

            for (let x = 0; x < this.map.width; x++) {
                let tile = this.map.getTileAt(x, y, null, "collisioni");
                gridCol.push(tile?.index);

                if (tile?.properties.length > 0 && tile?.properties.npc_point)
                    npcPointsCol.push(tile?.index);                    
            }

            this.npcPointsTiles.push(npcPointsCol);
            this.grid.push(gridCol);
        }
        
        this.finder.setGrid(this.grid);
        this.finder.disableDiagonals();
    }

    setAcceptableTiles() {
        let tileset = this.collisionsTileset;
        let properties = tileset.tileProperties;
        let acceptableTiles = [];

        // firstgid and total are fields from Tiled that indicate the range of IDs that the tiles can take in that tileset
        for (let i = tileset.firstgid - 1; i < tileset.total; i++) { 
            if (!properties.hasOwnProperty(i)) {
                // If there is no property indicated at all, it means it's a walkable tile
                acceptableTiles.push(i + 1);
                continue;
            }
            if (!properties[i].collides) acceptableTiles.push(i + 1);
        }
        this.finder.setAcceptableTiles(acceptableTiles);
    }

    setNpcPoints() {
        let tileset = this.collisionsTileset;
        let properties = tileset.tileProperties;
        
        // firstgid and total are fields from Tiled that indicate the range of IDs that the tiles can take in that tileset
        for (let i = tileset.firstgid - 1; i < tileset.total; i++) { 
            if (properties[i]?.npc_point) this.npcPointsTiles.push(i + 1);
        }

        for (let y = 0; y < this.map.height; y++) {
            for (let x = 0; x < this.map.width; x++) {
                let tile = this.map.getTileAt(x, y, null, "collisioni");

                if (this.npcPointsTiles.includes(tile?.index)) {
                    this.npcPoints.push({x: x, y: y});          
                    this.occupiedPoints[Phaser.Math.Clamp(this.npcPoints.length, 0, 100)] = false;
                }        
            }
        }
    }

    public move(fromX: number, fromY: number, toX: number, toY: number): object[] {
        let finalPath = [];
        
        this.finder.findPath(fromX, fromY, toX, toY, function( path ) {
            if (path === null) {
                console.warn("Path was not found.");
            } 
            else {
                path.forEach((point) => { finalPath.push({x: point.x, y: point.y}) })
                finalPath = lodash.cloneDeep(path);
            }
        });
        this.finder.calculate();        
        return finalPath;
    }
}