import { Globals } from "./GameData";

export default class CI {
    private _femaleNames = [
        "Maria", "Emanuela", "Giovanna", 
        "Raffaella", "Alessandra", "Anna", 
        "Ortensia", "Francesca", "Morgana", 
        "Valeria", "Marta", "Claudia", 
        "Benedetta", "Giulia", "Nicole",
        "Svetlana", "Serena", "Barbara"
    ];

    private _maleNames = [
        "Mario", "Emmanuel", "Giacomo", 
        "Nicola", "Giovanni", "Pasquale", 
        "Rodolfo", "Michele", "Federico", 
        "Cristian", "Simone", "Niccolò", 
        "Alfredo", "Valerio", "Francesco",
        "Lucio", "Nino", "Giuseppe", "George",
        "Igionio"
    ];

    private _surnames = [
        "Rossi", "Verdi", "Gatto", 
        "Manzo", "Rizzi", "Brancaccio",
        "De Paola", "Amato", "Avallone",
        "Senatore", "Paglietta", "Venutolo",
        "Galeazzi", "Cavallo", "Casaburi",
        "Fasano", "Ragone", "Verga",
        "Leopardi", "Vecchia", "Rosato",
        "Fava", "Russo", "Lupo", "D'Angelo",
        "Machiavelli", "Washington", "Massari",
        "D'Urso"
    ];

    name: string;
	surname: string;
    gender: string;
    birth: string;
    character: string;

    private _currentYear = new Date().getFullYear();

    private generaBirth (annoMax: number, annoMin: number) {
        let aaaa = Math.round(Math.random() * (annoMax - annoMin) + annoMin);
        let mm = CI.randomNumberTo(12) + 1;
        let gg = CI.randomNumberTo(28) + 1;

        return gg + "/" + mm + "/" + aaaa;
    }

    static randomNumberTo (to: number) {
        if (to == 0) return 0;
        return Math.round(Math.random() * 34375 % (to - 1));
    }
 
    private generateData () {
        if(Math.round(Math.random()) == 0) {
            this.name = this._femaleNames[CI.randomNumberTo(this._femaleNames.length)];
            this.gender = "Donna";
            this.character = "ch_f_" + CI.randomNumberTo(Globals.femaleCharacters);
        }
        else {
            this.name = this._maleNames[CI.randomNumberTo(this._maleNames.length)];
            this.gender = "Uomo";
            this.character = "ch_m_" + CI.randomNumberTo(Globals.maleCharacters);
        }
        this.surname = this._surnames[CI.randomNumberTo(this._surnames.length)];
        this.birth = this.generaBirth(this._currentYear - 18, 1970);
    }

    constructor() {
        this.generateData();
    }
}