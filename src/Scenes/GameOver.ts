import Phaser from "phaser";
import CI from "../CI";
import { Globals } from "../GameData";
import Main from "./Main";

export default class GameOver extends Phaser.Scene {
    randomMsgs = ["Mi hai deluso", "Domani ti lincenziamo", "Farai meglio a trovarti un nuovo lavoro", "Hai diversamente vinto!"]
    static i = 0;
    
    constructor() { super("GameOver" + GameOver.i++);  }

    create() {
        this.add.image(1440/2, 1024/2, "check_background").setScale(100, 100);

        let btn = this.add.image(1440/2, 600, "restart_btn").setOrigin(0.5, 0.5).setScale(7).setInteractive()
        .on('pointerdown', () => {
            btn.setTexture("restart_btn_pressed");
            this.scene.stop();
            this.scene.add("Main", Main, true);
        })
        .on('pointerover', () => {
            this.tweens.add({
                targets: [btn],
                scale: 7.5,
                duration: 250,
                ease: 'Power1',
            });
        })
        .on('pointerout', () => {
            this.tweens.add({
                targets: [btn],
                scale: 7,
                duration: 250,
                ease: 'Power1',
            });
            btn.setTexture("restart_btn");
        });

        this.add.text(1440/2, 400, this.randomMsgs[CI.randomNumberTo(this.randomMsgs.length)], Globals.normalFont).setOrigin(0.5, 0.5);
    }
}