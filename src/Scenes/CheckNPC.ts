import CI from "../CI";
import { Globals } from "../GameData";
import NPC from "../GameObjects/NPC";
import NPCsManager from "../NPCsManager";
import Main from "./Main";

export class CheckNPC extends Phaser.Scene {
    mainScene: Main;
    NPCBeingChecked: NPC;
    NPCsManager: NPCsManager;
    validNPC: boolean = true;
    openCISound: Phaser.Sound.BaseSound;
    wrongSound: Phaser.Sound.BaseSound;
    rightSound: Phaser.Sound.BaseSound;

    static i = 0;

    constructor(data) { 
        super({ key: "CheckNPC" + CheckNPC.i++ });
        this.NPCBeingChecked = data.NPC;
        this.mainScene = data.scene;
        this.NPCsManager = data.NPCsManager;
    }
    
    create() {
        this.add.image(1440/2, 1024/2, "check_background").setOrigin(0.5, 0.5).setScale(15);

        this.wrongSound = this.sound.add("wrong_sfx");
        this.rightSound = this.sound.add("right_sfx");

        this.createNPCBg();
        this.createCI();
        this.createPhone();
        this.createNotValidButton();
        this.createValidBtn();
    }

    randomChanche(chanche: number): boolean {
        return Math.random() < chanche / 100;
    } 

    createValidBtn() {
        let btn = this.add.image(556, 700, "valid_btn").setOrigin(0.5, 0.5).setScale(7).setInteractive()
        .on('pointerdown', () => { 
            btn.setTexture("valid_btn_pressed");
            this.checkAnswer(true);
        })
        .on('pointerover', () => {
            this.tweens.add({
                targets: [btn],
                scale: 7.5,
                duration: 250,
                ease: 'Power1',
            });
        })
        .on('pointerout', () => {
            this.tweens.add({
                targets: [btn],
                scale: 7,
                duration: 250,
                ease: 'Power1',
            });
            btn.setTexture("valid_btn");
        });
    }

    checkAnswer(answer: boolean) {
        if (answer != this.validNPC) {
            this.mainScene.wrongGP += 1;
            this.wrongSound.play();
        }
        if (answer == false) {
            this.NPCsManager.checkedNpc.goToDoor();
            this.NPCBeingChecked.headIconWrong.timer=0;
        }
        if (answer == this.validNPC) {
            this.rightSound.play();
            this.NPCBeingChecked.headIconChecked.timer=0;
        }
        this.mainScene.npcsToSpawn-=1;
        this.NPCsManager.checkedNpc.checked = true;
        this.NPCsManager.checkedNpc.beingChecked = false;
        this.NPCsManager.checkedNpc = null;
        this.scene.stop();
    }

    createNotValidButton() {
        let btn = this.add.image(356, 700, "not_valid_btn").setOrigin(0.5, 0.5).setScale(7).setInteractive()
        .on('pointerdown', () => { 
            btn.setTexture("not_valid_btn_pressed");
            this.checkAnswer(false);
        })
        .on('pointerover', () => {
            this.tweens.add({
                targets: [btn],
                scale: 7.5,
                duration: 250,
                ease: 'Power1',
            });
        })
        .on('pointerout', () => {
            this.tweens.add({
                targets: [btn],
                scale: 7,
                duration: 250,
                ease: 'Power1',
            });
            btn.setTexture("not_valid_btn");
        });
    }

    createNPCBg() {
        this.add.image(460, 1024/2 - 80, "check_background").setOrigin(0.5, 0.5).setScale(5);

        let npcClone = new NPC({ 
            scene: this, 
            CI: this.NPCBeingChecked.CI, 
            character: this.NPCBeingChecked.character,
            x: 460,
            y: 1024/2 - 80
        }).setScale(8).setOrigin(0.5, 0.5);
    }

    createCI() {
        let rightImg = this.randomChanche(75);

        let ci: CI = this.NPCBeingChecked.CI;

        if (!rightImg) {
            this.validNPC = false;
            ci = new CI();
        }

        this.add.image(710, 270, "CI").setOrigin(0, 0.5).setScale(7);
        this.add.image(990, 195, ci.character + "_ci").setOrigin(0, 0.5).setScale(6);
        this.add.text(735, 130, `${ci.name}\n${ci.surname}`, Globals.smallFont);
        this.add.text(735, 260, `${ci.gender}`, Globals.smallFont);
        this.add.text(735, 365, `${ci.birth}`, Globals.smallFont);
    }
    
    createPhone() {
        let rightQR = this.randomChanche(55);
        let rightName = this.randomChanche(95);
        let rightSurname = this.randomChanche(95);
        let rightGender = this.randomChanche(95);
        let rightBirth = this.randomChanche(95);

        let ci: CI = this.NPCBeingChecked.CI;
        let qr = "valid_QR";

        if (!rightQR || !rightName || !rightSurname || !rightGender || !rightBirth) {
            this.validNPC = false;
            if (!rightQR) qr = "not_valid_QR";
            if (!rightName) ci.name = new CI().name;
            if (!rightSurname) ci.surname = new CI().surname;
            if (!rightGender) ci.gender = new CI().gender;
            if (!rightBirth) ci.birth = new CI().birth;
        }

        this.add.image(710, 730, "phone").setOrigin(0, 0.5).setScale(7);
        this.add.image(830, 680, qr).setOrigin(0, 0.5).setScale(0.07);
        this.add.text(820, 780, `${ci.name} ${ci.surname}`, Globals.smallFont);
        this.add.text(820, 870, `${ci.gender}`, Globals.smallFont);
        this.add.text(920, 870, `${ci.birth}`, Globals.smallFont);
    }
    
}