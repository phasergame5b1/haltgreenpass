import Phaser from "phaser";
import { Globals } from "../GameData";
import Main from "./Main";

export default class Victory extends Phaser.Scene {
    static i = 0;
    
    constructor() { super("Victory" + Victory.i++);  }

    create() {
        this.add.image(1440/2, 1024/2, "check_background").setScale(100, 100);

        let btn = this.add.image(1440/2, 600, "restart_btn").setOrigin(0.5, 0.5).setScale(7).setInteractive()
        .on('pointerdown', () => {
            btn.setTexture("restart_btn_pressed");
            this.scene.stop();
            this.scene.add("Main", Main, true);
        })
        .on('pointerover', () => {
            this.tweens.add({
                targets: [btn],
                scale: 7.5,
                duration: 250,
                ease: 'Power1',
            });
        })
        .on('pointerout', () => {
            this.tweens.add({
                targets: [btn],
                scale: 7,
                duration: 250,
                ease: 'Power1',
            });
            btn.setTexture("restart_btn");
        });

        this.add.text(1440/2, 400, "Complimenti hai vinto! Puoi riprovare se ti va :D", Globals.normalFont).setOrigin(0.5, 0.5);
    }
}