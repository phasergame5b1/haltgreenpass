import Preloader from "./Preloader";

export default class Boot extends Phaser.Scene {
    constructor() {
      super({ key: "Boot" });
    }
  
    preload() {
      this.load.image("logo", "./Assets/logo.png");
      /* sì, ehm, si deve convertire in font bitmap prima quindi per ora lo saltiamo
      this.load.bitmapFont(
        "arcade",
        "assets/fonts/arcade.png",
        "assets/fonts/arcade.xml"
      );*/
  
      var graphics = this.make.graphics({ x: 0, y: 0, add: false });
  
      graphics.fillStyle(0x3A3A50, 1);
      graphics.fillRect(0, 0, 1440, 1024);
      graphics.generateTexture("black-screen", 1440, 1024);
    }
    
    create() { this.scene.add("Preloader", Preloader, true); }
  
  }
  