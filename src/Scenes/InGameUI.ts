import TextValue from '../GameObjects/TextValue';
import TextTimer from '../GameObjects/TextTimer';
import Main from './Main';
import { Globals } from '../GameData';

export class InGameUI extends Phaser.Scene {

  private _npcs!: TextValue;
  private _timer!: TextTimer;
  private _uncheckedGreenPasses!: TextTimer;
  mainScene: Main; 
  static i = 0;

  constructor(mainScene) {
    super('InGameUI' + InGameUI.i++);
    this.mainScene = mainScene;
  }
  
  create(): void {
    this.add.image(100, 150, "remaining_clients").setScale(3);
    this.add.image(100, 245, "clock").setScale(3);
    this.add.image(110, 345, "unchecked_green_passes").setScale(3);

    this._npcs = new TextValue(this, 160, 130, 15);
    this._timer = new TextTimer(this, 160, 240);
    this._uncheckedGreenPasses = new TextValue(this, 160, 335, 0);


    let decreaseTimer = () => { 
      let timerIsRunning: boolean = this._timer.decreaseTimer();
      if(!timerIsRunning)
        return; // per ora non fa nulla ma dovrà far finire la giornata
    };

    this.time.addEvent({
      delay: 1000,
      callback: decreaseTimer,
      callbackScope: this,
      loop: true
    });
  }

  update(time: number, delta: number): void {
    let secs = this._timer.getSeconds();

    this._uncheckedGreenPasses.setText(this.mainScene.wrongGP.toString());
    this._npcs.setText(this.mainScene.npcsToSpawn.toString());

    if (secs > Globals.matchSeconds / 3 && secs < Globals.matchSeconds/2)
      this.mainScene.music.setRate(1.12);
    if (secs > Globals.matchSeconds / 4 && secs < Globals.matchSeconds/3)
      this.mainScene.music.setRate(1.16);
  }
}