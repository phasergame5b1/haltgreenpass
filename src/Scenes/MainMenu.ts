import Phaser from "phaser";
import Main from "./Main";

export default class MainMenu extends Phaser.Scene {
    private _popupBg: Phaser.GameObjects.Image; 
    private _title: Phaser.GameObjects.Image;
    private _playBtn: Phaser.GameObjects.Image;
    private _muteBtn: Phaser.GameObjects.Image;
    private _sign: Phaser.GameObjects.Image;
    private _hands: Phaser.GameObjects.Image;

    music;

    constructor() {
        super({key: "MainMenu"});
    }
    
    create() {        
        this.add.image(0, 0, "menu_bg").setOrigin(0.4, 0).setScale(1.5);
        this._popupBg = this.add.image(1440/2, 1024/2, "popup_menu_bg").setOrigin(0.5, 0.5).setScale(0.9);
        this._sign = this.add.image(345, 1024/2 + 9, "halt_sign").setOrigin(0.5, 0.5);
        this._title = this.add.image(728, -5, "title").setOrigin(0.5, 0.5);
        this._hands = this.add.image(720, 1024+10, "logo").setOrigin(0.5, 0.5).setScale(1); // y: 470 da alzate, 490 da abbassate
        
        this.music = this.sound.add("menu_music");
        this.music.play(null);
        this.music.loop = true;
    
        // PLAY
        this._playBtn = this.add.image(905, 1024+10, "play_btn").setOrigin(0.5, 0.5).setScale(1).setInteractive()
        .on('pointerdown', () => { 
            this._playBtn.setTexture("play_btn_pressed");
            this.music.stop();
            this.scene.stop();
            this.scene.add("Main", Main, true);
        })
        .on('pointerover', () => {
            this.tweens.add({
                targets: [this._hands],
                y: 470,
                duration: 250,
                ease: 'Power1',
            });
            this.tweens.add({
                targets: [this._hands],
                scale: 11.5,
                duration: 250,
                ease: 'Power1',
            });
            this.tweens.add({
                targets: [this._playBtn],
                scale: 10.5,
                duration: 250,
                ease: 'Power1',
            });
        })
        .on('pointerout', () => {
            this.tweens.add({
                targets: [this._hands],
                y: 500,
                duration: 250,
                ease: 'Power1',
            });
            this.tweens.add({
                targets: [this._playBtn],
                scale: 10,
                duration: 250,
                ease: 'Power1',
            });
            this.tweens.add({
                targets: [this._hands],
                scale: 10,
                duration: 250,
                ease: 'Power1',
            });
            this._playBtn.setTexture("play_btn")
        });

        // MUTE
        this._muteBtn = this.add.image(1180, 1024+10, "audio_on").setOrigin(0.5, 0.5).setScale(1).setInteractive().
        on('pointerdown', () => { 
            this._muteBtn.setTexture("audio_off")
            // mettere un booleano: this._muteBtn.setTexture("audio_on")
        })
        .on('pointerover', () => {
            this.tweens.add({
                targets: [this._muteBtn],
                scale: 2.3,
                duration: 250,
                ease: 'Power1',
            });
        })
        .on('pointerout', () => {
            this.tweens.add({
                targets: [this._muteBtn],
                scale: 2.1,
                duration: 250,
                ease: 'Power1',
            });
        });;

        this.setAnimations();

    }

    update(time: number, delta: number): void {
    }

    setAnimations() {
        // Allarga finestra popup
        this.tweens.add({
            targets: [this._popupBg],
            scale: 1,
            duration: 250,
            ease: 'Power1',
        });
        // Muovi titolo giù
        this.tweens.add({
            targets: [this._title],
            y: 285,
            duration: 250,
            ease: 'Power1',
        });
        // Scala cartello
        this.tweens.add({
            targets: [this._sign],
            scale: 11.6,
            duration: 250,
            ease: 'Power1',
        });
        // Muovi bottone play
        this.tweens.add({
            targets: [this._playBtn],
            y: 677,
            duration: 250,
            ease: 'Power1',
        });
        // Scala bottone play e mani
        this.tweens.add({
            targets: [this._playBtn, this._hands],
            scale: 10,
            duration: 250,
            ease: 'Power1',
        });
        // Muovi mani su
        this.tweens.add({
            targets: [this._hands],
            y: 480,
            duration: 250,
            ease: 'Power1',
        });
        // Muovi mani giù
        this.tweens.add({
            targets: [this._hands],
            y: 500,
            duration: 250,
            ease: 'Power1',
        });
        // Muovi btn audio
        this.tweens.add({
            targets: [this._muteBtn],
            y: 485,
            duration: 250,
            ease: 'Power1',
        });
        // Scala audio
        this.tweens.add({
            targets: [this._muteBtn],
            scale: 2.1,
            duration: 250,
            ease: 'Power1',
        });
        // Scala audio
        this.tweens.add({
            targets: [this._title],
            scale: 1.01,
            yoyo: true,
            repeat: -1,
            duration: 400,
            ease: 'Power1',
        });
    }
}