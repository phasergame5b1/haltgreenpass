
import { GameObjects, Physics, Tilemaps } from "phaser";
import NPCsManager from "../NPCsManager";
import Pathfinding from "../Pathfinder";
import { CheckNPC } from "./CheckNPC";
import GameOver from "./GameOver";
import { InGameUI } from "./InGameUI";
import Victory from "./Victory";

export default class Main extends Phaser.Scene {
    NPCs: NPCsManager;
    map: Tilemaps.Tilemap;
    tilesets = ["door", "jail", "basement", "floor", "grocery", "hospital", "grass", "walls"];
    structGroup: Physics.Arcade.Group;
    maxErrors = 1;

    // Layers
    tile6; 
    lvl1;
    structures;
    barriers;
    food1; 
    food2;
    windows;
    collisions;
    
    collisionTileset;
    pathfinder: Pathfinding;
    mainMenu;
    wrongGP = 0;
    npcsToSpawn = 13;
    music;

    isDead = false;

    static i = 0;

    constructor() {
        super({ key: "Main" + Main.i++});
    }

    create() {
        this.structGroup = this.physics.add.group();
        this.scene.add("UI-scene", new InGameUI(this), true);
        this.createMap();
        this.createStructures();
        this.structGroup.setDepth(400);
        this.pathfinder = new Pathfinding({scene: this, map: this.map, collisionsTileset: this.collisionTileset})
        this.NPCs = new NPCsManager(this, this.structGroup);

        this.physics.add.collider(this.NPCs.player, this.collisions);
        this.music = this.sound.add("map_music");
        this.music.play(null);
        this.music.loop = true;

        this.input.on("pointerdown", () => { 
            //this.NPCs.player.moveTo(this.pathfinder, this.map.worldToTileX(this.input.mousePointer.worldX), this.map.worldToTileY(this.input.mousePointer.worldY);
            console.log("x: " + this.input.mousePointer.worldX + " y: " + this.input.mousePointer.worldY);
        }, this)

        //let checkMenu = new CheckNPC({ scene: this, NPC: this.NPCs.npcs[0] });
        //this.scene.add("CheckNPC", checkMenu, true);
    }

    
    update(time: number, delta: number): void {
        this.NPCs.update(delta);
        this.NPCs.player.move(this.map);

        if (!this.isDead) {
            if (this.wrongGP > this.maxErrors) {
                console.log("troppi errori");
                this.gameOver();
            }
            if (this.npcsToSpawn == 0) {
                console.log("npc spawnati 0");
                this.win();
            }
        }
    }

    gameOver() {
        this.music.stop();
        this.isDead = true;
        //this.mainMenu.music.stop();
        this.scene.add("GameOver", GameOver, true);
    }

    win() {
        this.music.stop();
        this.isDead = true;
        //this.mainMenu.music.stop();
        this.scene.add("Victory", Victory, true);
    }

    createMap() {
        this.map = this.make.tilemap({ key: 'map' });

        this.map.addTilesetImage('door');
        this.map.addTilesetImage('jail');
        this.map.addTilesetImage('basement');
        this.collisionTileset = this.map.addTilesetImage('floor');
        this.map.addTilesetImage('walls');
        this.map.addTilesetImage('hospital');
        this.map.addTilesetImage('grass');
        this.map.addTilesetImage('grocery');

        this.tile6 = this.map.createLayer('Livello tile 6', this.tilesets);
        this.lvl1 = this.map.createLayer('lvl 1', this.tilesets);
        this.structures = this.map.createLayer('strutture', this.tilesets);
        this.barriers = this.map.createLayer('bariere', this.tilesets);
        this.food1 = this.map.createLayer('cibo1', this.tilesets);
        this.food2 = this.map.createLayer('cibo2', this.tilesets);
        this.windows = this.map.createLayer('sportelli/finestre', this.tilesets);
        this.collisions = this.map.createLayer('collisioni', this.tilesets).setCollisionFromCollisionGroup();

        //this.physics.add.collider(this.NPCs.player, this.collisions, null, null, this);
    }

    
    createStructures() {
        //barriers positions(collosion box done)
        this.structGroup.create(205.5, 265, 'barriers').setScale(0.9, 1).setOrigin(0, 0).setSize(123, 15);
        this.structGroup.create(205.5, 313, 'barriers').setScale(0.9, 1).setOrigin(0, 0).setSize(123, 15);

        //front barriers positions
        for (let index: number = 0, startingYFrontBarriers: number = 595; index < 3; index++, startingYFrontBarriers -= 54) {
            this.structGroup.create(45, startingYFrontBarriers, 'front_barrier').setOrigin(0.5, 1).setSize(0, 0);
            this.structGroup.create(93, startingYFrontBarriers, 'front_barrier').setOrigin(0.5, 1).setSize(0, 0);
        }
        this.structGroup.create(205, 275, 'front_barrier').setOrigin(0.5, 1).setSize(0, 0);

        //baskets positions(collosion box done)
        this.structGroup.create(255, 325, 'baskets').setOrigin(0, 0).setSize(14, 21);
        this.structGroup.create(255, 452, 'baskets').setOrigin(0, 0).setSize(14, 21);
        this.structGroup.create(255, 390, 'basket').setOrigin(0, 0).setSize(14, 15);
        this.structGroup.create(255, 518, 'basket').setOrigin(0, 0).setSize(14, 15);
    
        //cash boxs locations(collosion box done)
        this.structGroup.create(236.5, 351, 'cash_box').setOrigin(0.5, 0.5).setSize(41, 30);
        this.structGroup.create(236.5, 416, 'cash_box').setOrigin(0.5, 0.5).setSize(41, 30);
        this.structGroup.create(236.5, 479, 'cash_box').setOrigin(0.5, 0.5).setSize(41, 30);
        this.structGroup.create(236.5, 544, 'cash_box').setOrigin(0.5, 0.5).setSize(41, 30);

        //alarms locations(collosion box done)
        this.structGroup.create(198.5, 368, 'alarm').setOrigin(0.5, 0.5).setSize(16, 29);
        this.structGroup.create(198.5, 432.5, 'alarm').setOrigin(0.5, 0.5).setSize(16, 29);
        this.structGroup.create(198.5, 496.5, 'alarm').setOrigin(0.5, 0.5).setSize(16, 29);
        this.structGroup.create(198.5, 560.5, 'alarm').setOrigin(0.5, 0.5).setSize(16, 29);

        //reception location(collosion box done)
        this.structGroup.create(275, 225, 'reception').setOrigin(0, 1).setOrigin(0.5, 0.8).setSize(160, 60);
    
        //freez food locations(collosion box done)
        this.structGroup.create(915, 485, 'freez_food_1').setOrigin(0.5, 0.5).setScale(1, 1).setSize(30, 22).setOffset(2.75, 0.5);
        this.structGroup.create(867, 485, 'freez_food_1').setOrigin(0.5, 0.5).setScale(1, 1).setSize(30, 22).setOffset(2.75, 0.5);
        this.structGroup.create(819, 485, 'freez_food_1').setOrigin(0.5, 0.5).setScale(1, 1).setSize(30, 22).setOffset(2.75, 0.5);
        this.structGroup.create(913, 564, 'freez_food_2').setOrigin(0.5, 0.5).setScale(1, 1).setSize(30, 22);
        this.structGroup.create(865, 564, 'freez_food_2').setOrigin(0.5, 0.5).setScale(1, 1).setSize(30, 22);
        this.structGroup.create(817, 564, 'freez_food_2').setOrigin(0.5, 0.5).setScale(1, 1).setSize(30, 22);

        //fruit counter locations(collosion box done)
        this.structGroup.create(656, 98, 'fruit_1').setOrigin(0.5, 0.5).setScale(0.9, 0.9).setSize(192, 33);
        this.structGroup.create(657.5, 164, 'fruit_2').setOrigin(0.5, 0.5).setScale(0.9, 0.9).setSize(192, 33).setOffset(3.75, 3.5);
        this.structGroup.create(657.5, 225, 'fruit_3').setOrigin(0.5, 0.5).setScale(0.9, 0.9).setSize(192, 33);

        //medium shelf locations(collosion box done)
        this.structGroup.create(535, 477.5, 'shelf_front_medium_2').setOrigin(0.5, 0.5).setScale(1.1, 1).setSize(74, 38);
        this.structGroup.create(639.5, 477.5, 'shelf_front_medium_1').setOrigin(0.5, 0.5).setScale(1.1, 1).setSize(74, 38).setOffset(4,5);
        this.structGroup.create(728, 477.5, 'shelf_front_medium_2').setOrigin(0.5, 0.5).setScale(1.1, 1).setSize(74, 38);

        this.structGroup.create(589.5, 285.5, 'shelf_front_medium_1').setOrigin(0.5, 0.5).setScale(1.1, 1).setSize(74, 38).setOffset(5,4);
        this.structGroup.create(801.5, 285.5, 'shelf_front_medium_3').setOrigin(0.5, 0.5).setScale(1.08, 1).setSize(89, 38);

        //little shelf locations(collosion box done)
        this.structGroup.create(673.5, 286, 'shelf_front_little_1').setOrigin(0.5, 0.5).setScale(1.21, 1).setSize(26, 38);
        this.structGroup.create(721, 286, 'shelf_front_little_2').setOrigin(0.5, 0.5).setScale(1.21, 1).setSize(26, 38);
        this.structGroup.create(510.5, 286.5, 'shelf_front_little_3').setOrigin(0.5, 0.5).setScale(1.21, 1).setSize(26, 38);
        this.structGroup.create(880, 286, 'shelf_front_little_4').setOrigin(0.5, 0.5).setScale(1.21, 1).setSize(26, 38);
        this.structGroup.create(928.5, 286, 'shelf_front_little_5').setOrigin(0.5, 0.5).setScale(1.21, 1).setSize(26, 38);

        //large shelf locations(collosion box done)
        this.structGroup.create(561, 558.5, 'shelf_front_large_1').setOrigin(0.5, 0.5).setScale(1.05, 1).setSize(122, 40);
        this.structGroup.create(703.5, 557.5, 'shelf_front_large_2').setOrigin(0.5, 0.5).setScale(1.06, 1).setSize(122, 40);
        

        //carts locations(collosion box done)
        this.structGroup.create(390, 420, 'side_cart_2').setOrigin(0.5, 0.5).setScale(1.1, 1.1).setSize(21, 25);;

        //side shelfs locations
        //top right and top center(collosion box done)
        this.structGroup.create(842, 137, 'shelf_side_little_1').setOrigin(0.5, 0.9).setScale(1, 1.2).setSize(0.00001, 0.00001);
        this.structGroup.create(910.5, 138, 'shelf_side_little_2').setOrigin(0.5, 0.9).setScale(1, 1.2).setSize(0.00001, 0.00001);
        this.structGroup.create(430.5, 140, 'shelf_side_little_2').setOrigin(0.5, 0.9).setScale(1, 1.2).setSize(0.00001, 0.00001);
        this.structGroup.create(495, 161, 'shelf_side_large_4').setOrigin(0.5, 0.9).setScale(1, 0.8).setSize(0.00001, 0.00001);
        this.structGroup.create(490, 208.5, 'shelf_side_little_1').setOrigin(0.5, 0.9).setScale(1, 1.2).setSize(21, 100).setOffset(5, -52);
        this.structGroup.create(840.5, 190, 'shelf_side_large_1').setOrigin(0.5, 0.8).setScale(1, 0.8).setSize(21, 150).setOffset(5, -55);
        this.structGroup.create(904, 190, 'shelf_side_large_2').setOrigin(0.5, 0.8).setScale(1, 0.8).setSize(21, 150).setOffset(5, -55);
        this.structGroup.create(423.5, 205, 'shelf_side_large_3').setOrigin(0.5, 0.9).setScale(1, 0.8).setSize(21, 160).setOffset(8, -60);

        //bottom center(collosion box done)
        this.structGroup.create(364, 510, 'shelf_side_little_1').setOrigin(0.5, 0.9).setScale(1, 1.2).setSize(0.00001, 0.00001);
        this.structGroup.create(433.5, 510.5, 'shelf_side_little_2').setOrigin(0.5, 0.9).setScale(1, 1.2).setSize(0.00001, 0.00001);
        this.structGroup.create(364, 545.5, 'shelf_side_little_1').setOrigin(0.5, 0.8).setScale(1, 1.2).setSize(21, 85).setOffset(4, -35);
        this.structGroup.create(432.5, 545.5, 'shelf_side_little_2').setOrigin(0.5, 0.8).setScale(1, 1.2).setSize(21, 85).setOffset(8, -35);

        this.structGroup.create(362.5, 415, 'shelf_side_large_1').setOrigin(0.5, 0.8).setScale(1, 0.8).setSize(21, 95);
        this.structGroup.create(426, 415, 'shelf_side_large_2').setOrigin(0.5, 0.8).setScale(1, 0.8).setSize(21, 95);
        
        //carts columns
        for (let index: number = 0, startingYFrontCarts = 595; index < 15; index++, startingYFrontCarts -= 10) {
            this.structGroup.create(25, startingYFrontCarts, 'front_cart').setOrigin(0.5, 1).setScale(1, 1).setSize(16, 25).setOffset(3.5, 6);
            this.structGroup.create(70, startingYFrontCarts, 'front_cart').setOrigin(0.5, 1).setScale(1, 1).setSize(16, 25).setOffset(3.5, 6);
        }

    }
}