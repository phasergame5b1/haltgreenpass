import MainMenu from "./MainMenu";
import { Globals } from "../GameData";
import GameOver from "./GameOver";


export default class Preloader extends Phaser.Scene {
    private _image: Phaser.GameObjects.Image;
    private _progress: Phaser.GameObjects.Graphics;
    private _loading: Phaser.GameObjects.Text;

    constructor() {
        super({ key: "Preloader" })
    }

    init() {
        this._image = this.add
          .image(
            1440/2,
            1024/2 - 150,
            "logo"
          )
          .setAlpha(0)
          .setScale(6);
    
        this.tweens.add({
          targets: [this._image],
          alpha: 1,
          duration: 500,
        });
    
        this._loading = this.add
          .text(1440 / 2, 1024/2 + 150, "Caricamento...", { font: "42px" } )
          .setAlpha(1)
          .setDepth(1001)
          .setOrigin(0.5, 1);
    }

    preload() {
        this.cameras.main.setBackgroundColor("#3A3A50");
        this._progress = this.add.graphics();
        this.loadAssets();        
    }

    loadAssets(): void {
        this.setLoaderCallbacks()

        // Loading assets
        this.load.image("audio_on", "./Assets/MainMenu/audio1.png");
        this.load.image("audio_off", "./Assets/MainMenu/audio2.png");
        this.load.image("halt_sign", "./Assets/MainMenu/cartello_halt.png");
        this.load.image("play_btn", "./Assets/MainMenu/play_button1.png");
        this.load.image("play_btn_pressed", "./Assets/MainMenu/play_button2.png");
        this.load.image("title", "./Assets/MainMenu/titolo.png");
        this.load.image("menu_bg", "./Assets/MainMenu/Sfondo.png");
        this.load.image("popup_menu_bg", "./Assets/MainMenu/Sfondo_popup.png");
        this.load.image("ch_collision_box", "./Assets/Characters/collision_box.png");
        this.load.image("remaining_clients", "./Assets/giocatori_rimasti.png");
        this.load.image("clock", "./Assets/orologio.png");
        this.load.image("unchecked_green_passes", "./Assets/green_pass_non_visti.png");
        this.load.image("check_background", "./Assets/CheckMenu/sfondo.png");
        this.load.image("CI", "./Assets/CheckMenu/CI.png");
        this.load.image("phone", "./Assets/CheckMenu/telefono.png");
        this.load.image("not_valid_QR", "./Assets/CheckMenu/non_valida.png");
        this.load.image("valid_QR", "./Assets/CheckMenu/valido.png");
        this.load.image("valid_btn", "./Assets/CheckMenu/btn_valido.png");
        this.load.image("not_valid_btn", "./Assets/CheckMenu/btn_non_valido.png");
        this.load.image("valid_btn_pressed", "./Assets/CheckMenu/btn_valido_premuto.png");
        this.load.image("not_valid_btn_pressed", "./Assets/CheckMenu/btn_non_valido_premuto.png");
        this.load.image("wrong", "./Assets/sbagliato.png");
        this.load.image("valid", "./Assets/spunta.png");
        this.load.image("logo_e", "./Assets/logo_E.png");
        this.load.image("restart_btn", "./Assets/btn_riavvia.png");
        this.load.image("restart_btn_pressed", "./Assets/btn_riavvia_pressed.png");

        this.loadTilesets();
        this.loadStructures();
        this.load.image('map_png', './Assets/Map/mappa.png')
        this.load.tilemapTiledJSON('map', './Assets/Map/mappa.json')

        this.load.spritesheet('player_idle', './Assets/Characters/player_idle.png', { frameWidth: 16, frameHeight: 26 });
        this.load.spritesheet('player_run', './Assets/Characters/player_run.png', { frameWidth: 16, frameHeight: 26 });

        this.loadCharactersSpritesheets();
        this.loadSounds();
    }

    loadSounds() {
      this.load.audio("menu_music", "Assets/Sounds/musicamenu1.mp3");
      this.load.audio("mask_warning", "Assets/Sounds/AvvisoMascherina.m4a");
      this.load.audio("open_ci_sfx", "Assets/Sounds/AperturaDocumento.mp3");
      this.load.audio("button_sfx", "Assets/Sounds/button.mp3");
      this.load.audio("right_sfx", "Assets/Sounds/confirm.mp3");
      this.load.audio("wrong_sfx", "Assets/Sounds/wrong.mp3");
      this.load.audio("map_music", "Assets/Sounds/Musica.mp3");
    }

    loadStructures() {
      //load alarm, cart, reception, cash box and baskets
      this.load.image('reception', './Assets/Map/structures/reception.png')
      this.load.image('alarm', './Assets/Map/structures/allarme.png');
      this.load.image('side_cart_1', './Assets/Map/structures/carrello_vista_laterale_1.png');
      this.load.image('side_cart_2', './Assets/Map/structures/carrello_vista_laterale_2.png');
      this.load.image('front_cart', './Assets/Map/structures/carrello_vista_frontale.png');
      this.load.image('cash_box', './Assets/Map/structures/cassa.png');
      this.load.image('baskets', './Assets/Map/structures/cesto_spesa_1.png');
      this.load.image('basket', './Assets/Map/structures/cesto_spesa_2.png');
      //this.load.image('coso', './Assets/Map/structures/coso.png');

      //load front shelfs
      //load medium shelfs
      this.load.image('shelf_front_medium_1', './Assets/Map/structures/scaffale_front_medio_1.png');
      this.load.image('shelf_front_medium_2', './Assets/Map/structures/scaffale_front_medio_2.png');
      this.load.image('shelf_front_medium_3', './Assets/Map/structures/scaffale_front_medio_3.png');

      //load little shelfs
      this.load.image('shelf_front_little_1', './Assets/Map/structures/scaffale_front_piccolo_1.png');
      this.load.image('shelf_front_little_2', './Assets/Map/structures/scaffale_front_piccolo_2.png');
      this.load.image('shelf_front_little_3', './Assets/Map/structures/scaffale_front_piccolo_3.png');
      this.load.image('shelf_front_little_4', './Assets/Map/structures/scaffale_front_piccolo_4.png');
      this.load.image('shelf_front_little_5', './Assets/Map/structures/scaffale_front_piccolo_5.png');

      //load large shelfs
      this.load.image('shelf_front_large_1', './Assets/Map/structures/scaffale_frontale_grande_1.png');
      this.load.image('shelf_front_large_2', './Assets/Map/structures/scaffale_frontale_grande_2.png');

      //fruit
      this.load.image('fruit_1', './Assets/Map/structures/struttura_ortofrutta_1.png');
      this.load.image('fruit_2', './Assets/Map/structures/struttura_ortofrutta_2.png');
      this.load.image('fruit_3', './Assets/Map/structures/struttura_ortofrutta_3.png');

      //load side shelfs
      //load large shelfs
      this.load.image('shelf_side_large_1', './Assets/Map/structures/scaffale_lat_grande_1.png');
      this.load.image('shelf_side_large_2', './Assets/Map/structures/scaffale_lat_grande_2.png');
      this.load.image('shelf_side_large_3', './Assets/Map/structures/scaffale_lat_grande_3.png');
      this.load.image('shelf_side_large_4', './Assets/Map/structures/scaffale_lat_grande_4.png');
      //load little shelfs
      this.load.image('shelf_side_little_1', './Assets/Map/structures/scaffale_lat_piccolo_1.png');
      this.load.image('shelf_side_little_2', './Assets/Map/structures/scaffale_lat_piccolo_2.png');

      //load freez food
      this.load.image('freez_food_1', './Assets/Map/structures/surgelati_1.png');
      this.load.image('freez_food_2', './Assets/Map/structures/surgelati_2.png');

      this.load.image('barriers', './Assets/Map/structures/transenne.png');
      this.load.image('front_barrier', './Assets/Map/structures/front_barrier.png')
    }

    loadTilesets() {
      this.load.image('door', './Assets/Map/tilesets/door.gif');
      this.load.image('jail', './Assets/Map/tilesets/jail.png');
      this.load.image('basement', './Assets/Map/tilesets/basement.png');
      this.load.image('grass', './Assets/Map/tilesets/grass.png');
      this.load.image('grocery', './Assets/Map/tilesets/grocery.png');
      this.load.image('hospital', './Assets/Map/tilesets/hospital.png');
      this.load.image('floor', './Assets/Map/tilesets/floor.png');
      this.load.image('walls', './Assets/Map/tilesets/walls.png');
    }

    loadCharactersSpritesheets() {
      for (let i = 0; i < Globals.maleCharacters; i++) {
        this.load.spritesheet('ch_m_' + i + '_idle', './Assets/Characters/ch_m_' + i + '_idle.png', { frameWidth: 16, frameHeight: 26 });
        this.load.spritesheet('ch_m_' + i + '_run', './Assets/Characters/ch_m_' + i + '_run.png', { frameWidth: 16, frameHeight: 26 });
        this.load.image('ch_m_' + i + '_ci', './Assets/Characters/ch_m_' + i + '_ci.png', { frameWidth: 16, frameHeight: 26 });
      }

      for (let i = 0; i < Globals.femaleCharacters; i++) {
        this.load.spritesheet('ch_f_' + i + '_idle', './Assets/Characters/ch_f_' + i + '_idle.png', { frameWidth: 16, frameHeight: 26 });
        this.load.spritesheet('ch_f_' + i + '_run', './Assets/Characters/ch_f_' + i + '_run.png', { frameWidth: 16, frameHeight: 26 });
        this.load.image('ch_f_' + i + '_ci', './Assets/Characters/ch_f_' + i + '_ci.png', { frameWidth: 16, frameHeight: 26 });
      }
    }

    setLoaderCallbacks() {
        this.load.on("start", () => { });

        this.load.on("fileprogress", (file: any, value: any) => {
          //console.log(file, value)
        });
    
        this.load.on("progress", (value: any) => {
          this._progress.clear();
          this._progress.fillStyle(0x64B63B, 1);
          // 200px spazio + barra di caricamento + 200 px spazio 
          this._progress.fillRect(200, 452, 1040 * value, 70);
          this._loading.setText("Caricamento...");
        });
    
        this.load.on("complete", () => {
          this._loading.setText("Clicca per giocare");
          //this.scene.add("MainMenu", MainMenu, true);
          this.input.once("pointerdown", () => {
            this.tweens.add({
              targets: [this._image, this._loading, this._progress],
              alpha: 0,
              duration: 500,
              onComplete: () => {
                this.scene.setVisible(false);
                this.scene.add("MainMenu", MainMenu, true);
              },
            });
          });
        });
    }
}