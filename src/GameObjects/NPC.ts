import Phaser from "phaser";
import CI from "../CI";
import Pathfinder from "../Pathfinder";
import Main from "../Scenes/Main";
import HeadIcon from "./HeadIcon";

export default class NPC extends Phaser.Physics.Arcade.Sprite {
    CI: CI;
    character: string;

    speed: integer = 80;
    velocity: Phaser.Math.Vector2 = new Phaser.Math.Vector2(0, 0);
    previousVelocity: Phaser.Math.Vector2 = new Phaser.Math.Vector2(0, 0);

    scene: Main;

    path = [];
    pathfinder: Pathfinder;
    currentPoint;
    exploredPoints = 0; 
    pointDelay = 22;
    pointsToExplore = 4;
    hasToDestroy = false;
    static = false;
    checked = false;
    beingChecked = false;
    headIconChecked: HeadIcon;
    headIconWrong: HeadIcon;
    headIconLogo: HeadIcon;

    // esempio: {scene: this, x: 100, y: 100, character: someCI.character, CI: someCI}
    constructor(data: any) {
        super(data.scene, data.x, data.y, "ch_collision_box");
        
        this.character = data.character;
        if ("static" in data) this.static = data.static;
        if ("CI" in data) this.CI = data.CI;
        
        this.setDepth(401);
        this.setIdleAnimations();
        this.setRunAnimations();
        this.setOrigin(0.5, 1);
        this.play(this.character + "_idle_down");

        this.pathfinder = data.pathfinder;
        if (data.exploresMap) this.exploreMap();

        data.scene.add.existing(this);
        data.scene.physics.add.existing(this);

        this.headIconLogo = new HeadIcon(this.scene, 0, -39, "logo_e", this);
        this.headIconChecked = new HeadIcon(this.scene, 0, -39, "valid", this);
        this.headIconWrong = new HeadIcon(this.scene, 0, -39, "wrong", this);
    }

    update(delta): void {
        this.setVelocity(this.velocity.x, this.velocity.y);
        this.setAnimation();
        this.move();
        this.setDepth(this.y - 13);
        this.headIconLogo.update(delta);
        this.headIconChecked.update(delta);
        this.headIconWrong.update(delta);


        let tileXY = this.scene.map.worldToTileXY(this.x, this.y);
        if (this.hasToDestroy && tileXY.x === 5 && tileXY.y === 3) {
            if (!this.checked) {
                this.scene.wrongGP++;
                this.scene.npcsToSpawn--;
            }
            this.headIconChecked.destroy();
            this.headIconWrong.destroy();
            this.headIconLogo.destroy();
            this.destroy();
        }
    }
    
    exploreMap() {
        if (this.hasToDestroy) return;

        this.leavePoint();
        this.occupyRandomPoint();

        let pointXY = this.pathfinder.npcPoints[this.currentPoint];
        this.moveTo(pointXY.x, pointXY.y);

        let delay = this.pointDelay + CI.randomNumberTo(25);
        this.scene.time.delayedCall(delay * 1000, () => {
            this.gotToTheNextPoint();
        }, null, this);
    }

    gotToTheNextPoint() {
        if (this.hasToDestroy) return;

        if (this.beingChecked) { 
            let delay = this.pointDelay + CI.randomNumberTo(15);

            this.scene.time.delayedCall(delay * 1000, () => {
                this.gotToTheNextPoint();
            }, null, this);
            return;
        }
                
        if (this.exploredPoints < this.pointsToExplore) {
            this.exploreMap();
            this.exploredPoints += 1;
        }
        else if (!this.hasToDestroy) {
            this.goToDoor();
        }
    }

    goToDoor() {
        this.pointsToExplore = 0;
        this.moveTo(5, 3);
        this.hasToDestroy = true;
    }

    leavePoint() {
        if (this.currentPoint)
            this.pathfinder.occupiedPoints[this.currentPoint] = false;
    }

    occupyRandomPoint() {
        let pointsLength = this.pathfinder.npcPoints.length;
        let availablePoints = [];
        
        for (let i = 0; i < pointsLength; i++)
            if (!this.pathfinder.occupiedPoints[i]) {
                availablePoints.push(i);
            }
        
        this.currentPoint = availablePoints[CI.randomNumberTo(availablePoints.length)];
        this.pathfinder.occupiedPoints[this.currentPoint] = true;
    }

    setIdleAnimations() {
        this.scene.anims.create({
            key: this.character + "_idle_up",
            frames: this.anims.generateFrameNumbers(this.character + "_idle", { start: 6, end: 11 }),
            frameRate: 8,
            repeat: -1,
        });
        this.scene.anims.create({
            key: this.character + "_idle_right",
            frames: this.anims.generateFrameNumbers(this.character + "_idle", { start: 0, end: 5 }),
            frameRate: 8,
            repeat: -1,
        });
        this.scene.anims.create({
            key: this.character + "_idle_left",
            frames: this.anims.generateFrameNumbers(this.character + "_idle", { start: 13, end: 17 }),
            frameRate: 8,
            repeat: -1,
        });
        this.scene.anims.create({
            key: this.character + "_idle_down",
            frames: this.anims.generateFrameNumbers(this.character + "_idle", { start: 18, end: 23 }),
            frameRate: 8,
            repeat: -1,
        });
    }

    setRunAnimations() {
        this.scene.anims.create({
            key: this.character + "_run_up",
            frames: this.anims.generateFrameNumbers(this.character + "_run", { start: 7, end: 11 }),
            frameRate: 8,
        }); 
        this.scene.anims.create({
            key: this.character + "_run_right",
            frames: this.anims.generateFrameNumbers(this.character + "_run", { start: 0, end: 5 }),
            frameRate: 8,
        });
        this.scene.anims.create({
            key: this.character + "_run_left",
            frames: this.anims.generateFrameNumbers(this.character + "_run", { start: 13, end: 17 }),
            frameRate: 8,
        });
        this.scene.anims.create({
            key: this.character + "_run_down",
            frames: this.anims.generateFrameNumbers(this.character + "_run", { start: 18, end: 23 }),
            frameRate: 8,
        });
    }

    moveTo(x: number, y: number) {
        let npcTileXY = this.pathfinder.map.worldToTileXY(this.x, this.y);
        this.path = this.pathfinder.move(npcTileXY.x, npcTileXY.y, x, y);
    }

    move() {
        if (this.path.length == 0 || this.beingChecked) { return; }

        let map = this.scene.map;
        let nextPoint = this.path[0];
        let npcTileXY = map.worldToTileXY(this.x, this.y);

        // se è arrivato al prossimo punto
        if (npcTileXY.x == nextPoint.x && npcTileXY.y == nextPoint.y)
            if (this.path.length > 1) {
                this.path = this.path.slice(1, this.path.length);
                nextPoint = this.path[0];
            }
            else if (this.path.length == 1) {
                this.path = [];
                this.velocity = new Phaser.Math.Vector2(0, 0);

                return;
            }
        
        let dir = new Phaser.Math.Vector2();
        if (nextPoint.x - npcTileXY.x > 0)
            dir.x = 1;
        else if (nextPoint.x - npcTileXY.x < 0)
            dir.x = -1;
        if (nextPoint.y - npcTileXY.y > 0)
            dir.y = 1;
        else if (nextPoint.y - npcTileXY.y < 0)
            dir.y = -1;

        this.velocity = dir.multiply(new Phaser.Math.Vector2(this.speed, this.speed));
    }

    setAnimation() {
        if (this.velocity.length() != 0)
            this.previousVelocity = this.velocity;
        
        if (this.velocity.y < 0)
        {
            this.play(this.character + "_run_up", true);
        }
        else if (this.velocity.x > 0)
        {
            this.play(this.character + "_run_right", true);
        }
        else if (this.velocity.x < 0){
            this.play(this.character + "_run_left", true);
        }
        else if (this.velocity.y > 0) {
            this.play(this.character + "_run_down", true);
        }
        else if (this.velocity.length() == 0) {
            if (this.previousVelocity.y > 0) {
                this.play(this.character + "_idle_down", true);
            }
            else if (this.previousVelocity.y < 0) {
                this.play(this.character + "_idle_up", true);
            }
            else if (this.previousVelocity.x > 0) {
                this.play(this.character + "_idle_right", true);
            }
            else if (this.previousVelocity.x < 0) {
                this.play(this.character + "_idle_left", true);
            }
        }

        this.velocity = this.velocity.normalize();
        this.velocity.multiply(new Phaser.Math.Vector2(this.speed, this.speed));
    }
}