import { GameObjects } from "phaser";
import { Globals } from "../GameData";

export default class TextValue extends GameObjects.Text {

  private _value: number;

  constructor(scene: Phaser.Scene, x: number, y: number, startValue: number = 0 ) {
    super(scene, x, y, `${startValue}`, Globals.normalFont);
    scene.add.existing(this);

    this.setDepth(400);
    this.setOrigin(0, 0);
    this._value = startValue;
  }

  public setValue(value: number) {
    this._value = value;
    this.setText(`${this._value}`);
  }

  public decreaseValue(value: number) {
    this._value -= value;
    this.setText(`${this._value}`);
  }

  public getValue(): number {
    return this._value;
  }
}