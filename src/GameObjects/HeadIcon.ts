import { Tweens } from "phaser";
import NPC from "./NPC";

export default class HeadIcon extends Phaser.GameObjects.Image {
    timer = 5000;
    npc: NPC;
    offsetX;
    offsetY;
    followPlayer: Tweens.Tween;

    constructor(scene, offsetX, offsetY, texture, npc) {
        super(scene, offsetX, offsetY, texture);
        this.npc = npc;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.scene.add.existing(this);
        this.setDepth(499); 
    }

    update(delta: number): void {
        this.timer += delta / 100;

        if (this.timer >= 5000)
            this.setAlpha(0);
        else 
            this.setAlpha(1);
        
        this.x = this.npc.x + this.offsetX;
        this.y = this.npc.y + this.offsetY;
    }
}