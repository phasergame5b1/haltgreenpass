import { GameObjects } from "phaser";
import { Globals } from "../GameData";
import GameOver from "../Scenes/GameOver";
import { InGameUI } from "../Scenes/InGameUI";

export default class TextClients extends GameObjects.Text {

  private _seconds: number;
  scene: InGameUI;

  constructor(scene: Phaser.Scene, x: number, y: number) {
    super(scene, x, y, "", Globals.normalFont);
    scene.add.existing(this);

    this._seconds = Globals.matchSeconds;
    this.updateTimerText();
    this.setDepth(400);
    this.setOrigin(0, 0);
  }

  public updateTimerText() {
    let hours = Math.floor(this._seconds / 3600);
    let minutes = Math.floor((this._seconds - (hours * 3600)) / 60);
    let seconds = this._seconds % 60;
    let zeroBeforeSecondsDigit = seconds < 10 ? "0" : "";

    this.setText(`${minutes}:${zeroBeforeSecondsDigit}${seconds}`);
  }

  public decreaseTimer(): boolean {
    if (this._seconds === 0 && !this.scene.mainScene.isDead) {
      this._seconds = -1;
      this.scene.mainScene.gameOver();
      return false; 
    }
    this._seconds -= 1;
    this.updateTimerText();
    return true;
  }

  public getSeconds(): number {
    return this._seconds;
  }
}