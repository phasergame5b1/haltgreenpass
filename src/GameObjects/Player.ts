import { Physics } from "phaser";
import NPC from "./NPC";

export default class Player extends NPC {
    character: string;
    keys: any;
    speed: integer = this.speed + 20;
    velocity: Phaser.Math.Vector2 = new Phaser.Math.Vector2(0, 0);
    previousVelocity: Phaser.Math.Vector2 = new Phaser.Math.Vector2(0, 0);
    fullBodyCollider: Physics.Arcade.Image;

    constructor(data: any) {
        super(data);
        
        this.keys = this.scene.input.keyboard.addKeys({
            up: 'w',
            down: 's',
            left: 'a',
            right: 'd'
        });

        this.fullBodyCollider = this.scene.physics.add.image(this.x, this.y - 13, "ch_m_0_ci").setAlpha(0);
        this.fullBodyCollider.setOrigin(0.5, 1);
        this.fullBodyCollider.setOffset(0, 13);
    }
    
    update(delta): void {
        this.setSize(10, 5);
        this.setOffset(3, 22);

        if (this.scene.NPCs.checkedNpc === null)
            this.getVelocity();
        else 
            this.velocity = new Phaser.Math.Vector2(0, 0);

        this.fullBodyCollider.x = this.x;
        this.fullBodyCollider.y = this.y - 13;
        super.update(delta);
    }

    getVelocity() {
        let k = this.keys;
        this.velocity = new Phaser.Math.Vector2(0, 0);
        
        if (k.up.isDown)
        {
            this.velocity.y -= 1;
        }
        else if (k.right.isDown)
        {
            this.velocity.x += 1;
        }
        else if (k.left.isDown){
            this.velocity.x -= 1;
        }
        else if (k.down.isDown) {
            this.velocity.y += 1;
        }

        this.velocity = this.velocity.normalize();
        this.velocity.multiply(new Phaser.Math.Vector2(this.speed, this.speed));
    }
}
